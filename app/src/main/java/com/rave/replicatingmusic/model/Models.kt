package com.rave.replicatingmusic.model

import androidx.annotation.DrawableRes
import com.rave.replicatingmusic.R


data class Comment(
    @DrawableRes val avatarId: Int = R.drawable.ic_launcher_foreground,
    val name: String = "Test Name",
    val text: String = "subtext string",
    val date: String = "Feb 14, 2023"
)

val commentList = listOf(
    Comment(name = "Bob Smith"),
    Comment(name = "Jerry Smith"),
    Comment(name = "Jill Smith"),
    Comment(name = "Kerry Smith"),
    Comment(name = "Barry Smith"),
    Comment(name = "Smarry Smith"),
    Comment(name = "Larry Smith"),
    Comment(name = "Parry Smith"),
    Comment(name = "Zarry Smith"),
    Comment(name = "Mary Smith")
)

val songList = listOf(
    "Give Life Back to Music",
    "The Game of Love",
    "Giorgio by Moroder",
    "Within",
    "Instant Crush",
    "Lose Yourself to Dance",
    "Touch",
    "Get Lucky",
    "Beyond",
    "Motherboard",
    "Fragments of Time",
    "Doin' It Right",
    "Contact"
)

