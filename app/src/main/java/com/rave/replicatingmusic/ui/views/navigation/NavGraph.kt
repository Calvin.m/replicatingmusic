package com.rave.replicatingmusic

import android.util.Log
import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController

@Composable
fun NavGraph() {
    val navController = rememberNavController()

    Log.e("MainScreen route: ", Screen.MainScreen.route)

    NavHost(
        navController = navController,
        startDestination = Screen.MainScreen.route
    ) {
        composable(Screen.MainScreen.route) {
//            MainScreen(navigate = {
//
//            })
//            MainScreen()
            MainScreen {
                navController.navigate(
                    Screen.AlbumScreen.route
                )
            }
        }
        composable(route = Screen.AlbumScreen.route) {
            AlbumScreen()
        }
    }
}