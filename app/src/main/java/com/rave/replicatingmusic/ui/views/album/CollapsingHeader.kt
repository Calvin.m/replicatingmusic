package com.rave.replicatingmusic

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp


//fun Modifier.collapsingHeaderController(
//    maxOffsetPx: Float,
//    firstVisibleItemIndexProvider: () -> Int,
//    onScroll: (currentOffsetY: Float) -> Unit,
//): Modifier = composed {
//    val scrollListener by rememberUpdatedState(newValue = onScroll)
//
//    val connection = remember {
//        object : NestedScrollConnection {
//
//        }
//    }
//    nestedScroll(connection)
//}


@Composable
fun CollapsingHeader() {
    Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
        Text(text = "Now Playing", color = Color.White, fontSize = 18.sp)
    }
    Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
        Box(
            Modifier
                .fillMaxWidth()
                .size(200.dp),
            contentAlignment = Alignment.Center
        ) {
            Image(
                painter = painterResource(R.drawable.daftpunk),
                contentDescription = null,
                modifier = Modifier
//                    .size(200.dp)
                    .clip(CircleShape)
            )
        }
    }
    Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Text(text = "Daft Punk", color = Color.White, fontSize = 36.sp)
            Text(text = "Random Access Memory", color = Color.White, fontSize = 24.sp)
        }
    }
//    HeaderTitle()
//    Box(content = HeaderTitle()) {
//        
//    }
//    SharedElementContainer(
//        title = HeaderTitle()
//    )
}

@Composable
fun SharedElementContainer(title: @Composable BoxScope.() -> Unit) {
    Box() {
        title()
    }
}


@Composable
private fun BoxScope.HeaderTitle(
//    alphaProvider: State<Float>,
//    onBackClick: () -> Unit = {},
//    onShareClick: () -> Unit = {},
) {
    // Top Menu
    Row(Modifier.align(Alignment.Center)) {
        Text(text = "Now Playing")
    }
}
