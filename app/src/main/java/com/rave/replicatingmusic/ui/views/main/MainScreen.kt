package com.rave.replicatingmusic

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.rave.replicatingmusic.musicpanels.MusicPanels
import com.rave.replicatingmusic.service.MusicService
import com.rave.replicatingmusic.ui.theme.ReplicatingMusicTheme


@Composable
fun MainScreen(
    navigate: () -> Unit
//startService: () -> Unit,
//stopService: () -> Unit
) {
    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        Row(
            modifier = Modifier
                .weight(0.6f)
                .fillMaxWidth()
        ) {
            MusicPanels(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                StatsScreen()
                PlayerScreen()
            }
        }
        Row(
            modifier = Modifier
                .weight(0.4f)
        ) {
            CommentsList(
                modifier = Modifier
                    .fillMaxSize()
            )
        }
        FloatingActionButton(
            onClick = {
                navigate()
            },
            backgroundColor = Color(0, 150, 136, 255),
            contentColor = Color.White
        ) {
            Image(
                painter = painterResource(R.drawable.ic_baseline_arrow_forward_ios_24),
                contentDescription = null,
                modifier = Modifier
                    .size(50.dp)
                    .padding(horizontal = 12.dp)
            )
        }
    }
}