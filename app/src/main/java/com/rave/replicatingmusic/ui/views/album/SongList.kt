package com.rave.replicatingmusic

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.rave.replicatingmusic.model.songList

@Composable
fun SongList(
//    modifier: Modifier = Modifier
) {
    LazyColumn {
        items(songList) { song ->
            Box(
                modifier = Modifier
                    .padding(1.dp)
                    .fillMaxWidth()
                    .height(50.dp)
                    .background(Color(182, 182, 182, 255)),
                contentAlignment = Alignment.Center
            ) {
                Text(text = song)
            }
        }
    }
}