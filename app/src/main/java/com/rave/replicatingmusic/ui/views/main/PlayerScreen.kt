package com.rave.replicatingmusic

import androidx.compose.animation.core.*
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.rave.replicatingmusic.ui.theme.ReplicatingMusicTheme
import kotlin.random.Random

@Composable
fun PlayerScreen() {
    RoundedCornersSurface(
        modifier = Modifier,
        color = Color(35, 35, 35),
        elevation = 5.dp,
        boxHeight = 400.dp
    ) {
        Column(
            Modifier
                .fillMaxWidth()
                .padding(12.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "Bob Smith",
                fontSize = 42.sp,
                color = Color.White
            )
            Column(Modifier.padding(top = 24.dp, start = 24.dp, end = 24.dp, bottom = 2.dp)) {
                Text(
                    text = "Philadelphia singer/songwriter is super bad and no one really likes their music.",
                    fontSize = 18.sp,
                    color = Color(218, 218, 218, 255)
                )
            }
            Column(Modifier.heightIn(min = 50.dp)) {
                Box(
                    modifier = Modifier
                        .padding(horizontal = 16.dp),
                    contentAlignment = Alignment.Center
                ) {
                    AnimatedWaveformBar(
                        modifier = Modifier
                            .fillMaxSize()
                            .height(48.dp)
                    )
                }
            }

        }
    }
//    RoundedCornersSurface(
//        modifier = Modifier,
//        color = Color(130,20,140),
//        elevation = 5.dp,
//        boxHeight = 400.dp
//    ) {
//        Box(
//            modifier = Modifier
//                .padding(horizontal = 16.dp).fillMaxWidth(),
//            contentAlignment = Alignment.Center
//        ) {
//            Column() {
//                Text(text = "Followers")
//                Text(text = "Blah")
//                Text(text = "Blahblah")
//            }
//        }
//    }

    /*
    MusicPanels(modifier = Modifier) {
         Box(
             modifier = Modifier
                 .padding(horizontal = 16.dp),
             contentAlignment = Alignment.Center
         ) {
         AnimatedWaveformBar(
             modifier = Modifier
                 .fillMaxSize()
                 .height(48.dp)
         )
     }
 }*/

}

@Composable
fun AnimatedWaveformBar(
    modifier: Modifier = Modifier,
    barWidth: Dp = 2.dp,
    gapWidth: Dp = barWidth,
    barColor: Color = MaterialTheme.colors.onPrimary,
//    isAnimating: Boolean = false,
) {
    val infiniteAnimation = rememberInfiniteTransition()
    val animations = mutableListOf<State<Float>>()
    val random = remember { Random(System.currentTimeMillis()) }
    var isAnimating by remember { mutableStateOf(false) }


    repeat(15) {
        val durationMillis = random.nextInt(500, 2000)
        animations += infiniteAnimation.animateFloat(
            initialValue = 0f,
            targetValue = 1f,
            animationSpec = infiniteRepeatable(
                animation = tween(durationMillis),
                repeatMode = RepeatMode.Reverse,
            )
        )
    }

//    val barWidthFloat by rememberUpdatedState(newValue = barWidth.toPxf(LocalDensity.current))
//    val gapWidthFloat by rememberUpdatedState(newValue = gapWidth.toPxf(LocalDensity.current))
    val barWidthFloat = 8f
    val gapWidthFloat = 8f

    val initialMultipliers = remember {
        mutableListOf<Float>().apply {
            repeat(100) { this += random.nextFloat() }
        }
    }

    val heightDivider by animateFloatAsState(
        targetValue = if (isAnimating) 1f else 3f,
        animationSpec = tween(1000, easing = LinearEasing)
    )

    Canvas(
        modifier
    ) {
        val canvasHeight = size.height
        val canvasWidth = size.width
        val canvasCenterY = canvasHeight / 2f

        val count =
            (canvasWidth / (barWidthFloat + gapWidthFloat)).toInt().coerceAtMost(100)
        val animatedVolumeWidth = count * (barWidthFloat + gapWidthFloat)
        var startOffset = (canvasWidth - animatedVolumeWidth) / 2
        val barMinHeight = 0f
        val barMaxHeight = canvasHeight / 2f / heightDivider

        repeat(count) { index ->
            val currentSize = animations[index % animations.size].value
            var barHeightPercent = initialMultipliers[index] + currentSize
            if (barHeightPercent > 1.0f) {
                val diff = barHeightPercent - 1.0f
                barHeightPercent = 1.0f - diff
            }
            val barHeight = lerpF(barMinHeight, barMaxHeight, barHeightPercent)

            drawLine(
                color = barColor,
                start = Offset(startOffset, canvasCenterY - barHeight / 2),
                end = Offset(startOffset, canvasCenterY + barHeight / 2),
                strokeWidth = barWidthFloat,
                cap = StrokeCap.Round,
            )
            startOffset += barWidthFloat + gapWidthFloat
        }
    }

    PlayPauseButton(isPlaying = isAnimating, onClick = {
//        if (isAnimating) {
//            startStopService()
//        } else {
//
//        }
        isAnimating = !isAnimating
    })
}

@Composable
fun PlayPauseButton(
    isPlaying: Boolean,
    onClick: () -> Unit = {},
) {
    IconButton(
        onClick = onClick,
        modifier = Modifier
            .size(32.dp)
            .background(color = Color(0, 136, 123, 255), shape = CircleShape)
    ) {
        Icon(
            painter = painterResource(id = if (isPlaying) R.drawable.ic_pause else R.drawable.ic_play),
            contentDescription = if (isPlaying) "Pause" else "Play",
            tint = MaterialTheme.colors.onPrimary, // white play button
        )
    }
}


@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ReplicatingMusicTheme {
        PlayerScreen()
    }
}