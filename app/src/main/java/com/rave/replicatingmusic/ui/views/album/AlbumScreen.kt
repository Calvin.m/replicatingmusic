package com.rave.replicatingmusic

import android.util.Log
import androidx.compose.foundation.layout.*
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController

@Composable
fun AlbumScreen(
    modifier: Modifier = Modifier,
) {
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        CollapsingHeader()

        SongList(
//            modifier = Modifier.collapsingHeaderController(
//                maxOffsetPx = 1f,
//                firstVisibleItemIndexProvider = { Log.e("firstVisItem", "$this") }
////                firstVisibleItemIndexProvider = { scrollState.firstVisibleItemIndex }
//            ) { currentScroll ->
//                Log.e("current scroll:", "$currentScroll")
//            }
        )

/*            SongList(
            modifier = Modifier
                .fillMaxSize()
                .offset(songsListOffsetProvider)
                .graphicsLayer { alpha = contentAlphaState.value }
                .collapsingHeaderController(
                    maxOffsetPx = headerState.maxHeaderCollapse.toPxf(),
                    firstVisibleItemIndexProvider = { scrollState.firstVisibleItemIndex },
                ) { currentScroll ->
                    headerState.headerHeight =
                        headerState.headerMaxHeight + currentScroll.toDp(density)
                },
            items = albumInfo.songs,
            bottomPadding = bottomControlsHeight + 24.dp,
            scrollState = scrollState,
            offsetPercent = listOffsetProgressState,
            likedIndices = likedIndices,
            onLikeClick = { clickedIndex ->
                likedIndices = likedIndices.onAction(clickedIndex)
            }
        )*/
    }

//        BottomPlayerControls(
//            modifier = Modifier
//                .fillMaxWidth()
//                .height(bottomControlsHeight)
//                .wrapContentSize(align = Alignment.BottomCenter)
//                .offset(controlsOffsetProvider),
//            bottomPadding = insets.bottomInset,
//        )
}