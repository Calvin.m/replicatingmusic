package com.rave.replicatingmusic

import androidx.compose.animation.core.animateDp
import androidx.compose.animation.core.keyframes
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.rave.replicatingmusic.model.commentList

val commentItemHeight = 50.dp

@Composable
fun CommentsList(modifier: Modifier) {
    LazyColumn {
        items(commentList) {
            BoxWithConstraints(
                modifier = modifier
                    .fillMaxWidth()
                    .background(color = Color(0, 136, 123, 255)),
                contentAlignment = Alignment.CenterEnd,
            ) {
                val (isActionPanelVisible, setIsActionPanelVisible) = remember {
                    mutableStateOf(
                        false
                    )
                }

                val density = LocalDensity.current
                val maxWidth = with(density) { constraints.maxWidth.toDp() }
                val maxHeight = commentItemHeight - 16.dp
                CommentListItem(
                    avatarId = it.avatarId,
                    name = it.name,
                    text = it.text,
                    date = it.date,
                    onClick = { setIsActionPanelVisible(true) }
                )
                ActionPanel(
                    modifier = Modifier
                        .align(Alignment.CenterEnd)
                        .padding(horizontal = 16.dp),
                    maxWidth = maxWidth,
                    maxHeight = maxHeight,
                    isVisible = isActionPanelVisible,
                    onActionClick = { setIsActionPanelVisible(false) }
                )
            }
        }
    }
}

@Composable
fun CommentListItem(
    avatarId: Int,
    name: String,
    text: String,
    date: String,
    onClick: () -> Unit
) {
    Row(modifier = Modifier
        .fillMaxWidth()
        .clickable { onClick() }
        .height(commentItemHeight)
        .background(Color(55, 55, 55)),
        verticalAlignment = Alignment.CenterVertically


    ) {
        Image(
            painter = painterResource(avatarId),
            contentDescription = null,
            modifier = Modifier.size(50.dp)
        )
        Column(
            modifier = Modifier
                .padding(start = 16.dp)
                .weight(1f)
        ) {
            Text(
                text = name,
                style = MaterialTheme.typography.subtitle1,
                color = Color.White
            )
            Text(
                text = text,
                style = MaterialTheme.typography.body2,
                color = Color.White
            )
        }
        Text(
            text = date,
            style = MaterialTheme.typography.body2,
            color = Color.White,
            modifier = Modifier.padding(end = 12.dp)
        )
    }
}

const val CORNER_DURATION = 300
const val EXPAND_DURATION = 300
const val MIN_RADIUS = 10

@Composable
fun ActionPanel(
    modifier: Modifier,
    maxWidth: Dp,
    maxHeight: Dp,
    isVisible: Boolean,
    onActionClick: () -> Unit
) {
    val stateTransition = updateTransition(targetState = isVisible, label = "Visibility")
    val targetWidth = stateTransition.animateDp(
        transitionSpec = {
            keyframes {
                durationMillis = CORNER_DURATION + EXPAND_DURATION
                if (targetState) {
                    maxHeight at CORNER_DURATION
                    maxWidth at CORNER_DURATION + EXPAND_DURATION
                } else {
                    maxHeight at EXPAND_DURATION
                    MIN_RADIUS.dp at EXPAND_DURATION + CORNER_DURATION
                }
            }
        },
        label = "Width"
    ) { value -> if (value) maxWidth else MIN_RADIUS.dp }

    val targetHeight by stateTransition.animateDp(
        transitionSpec = {
            keyframes {
                durationMillis = CORNER_DURATION + EXPAND_DURATION
                if (targetState) {
                    maxHeight at CORNER_DURATION
                } else {
                    maxHeight at EXPAND_DURATION
                    MIN_RADIUS.dp at EXPAND_DURATION + CORNER_DURATION
                }
            }
        },
        label = "Height"
    ) { value -> if (value) maxHeight else MIN_RADIUS.dp }

    val contentAlphaProvider = remember {
        { (targetWidth.value - MIN_RADIUS.dp) / (maxWidth - MIN_RADIUS.dp) }
    }

    Row(
        modifier = modifier
            .background(
                Color(0, 136, 123, 255),
                shape = RoundedCornerShape(16.dp)
            )
            .width(targetWidth.value)
            .height(targetHeight)
            .alpha(contentAlphaProvider()),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Image(
            painter = painterResource(R.drawable.ic_baseline_arrow_forward_ios_24),
            contentDescription = null,
            modifier = Modifier
                .size(50.dp)
                .padding(horizontal = 12.dp)
                .clickable { onActionClick() }
        )
        Image(
            painter = painterResource(R.drawable.ic_baseline_heart_broken_24),
            contentDescription = null,
            modifier = Modifier.size(50.dp)
        )
        Image(
            painter = painterResource(R.drawable.ic_baseline_outlined_flag_24),
            contentDescription = null,
            modifier = Modifier.size(50.dp)
        )
        Image(
            painter = painterResource(R.drawable.ic_baseline_restore_from_trash_24),
            contentDescription = null,
            modifier = Modifier.size(50.dp)
        )
    }
}