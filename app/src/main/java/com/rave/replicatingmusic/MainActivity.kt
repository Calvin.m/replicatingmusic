package com.rave.replicatingmusic

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Surface
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.rave.replicatingmusic.musicpanels.MusicPanels
import com.rave.replicatingmusic.service.MusicService
import com.rave.replicatingmusic.ui.theme.ReplicatingMusicTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ReplicatingMusicTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
//                    color = Color(15, 15, 15, 255)
                    color = Color(61, 61, 61, 255)
                ) {
//                    MainScreen()
                    NavGraph()
                }
            }
        }
    }
}
/*
    fun startStopService() {
        if (isMyServiceRunning(MusicService::class.java)) {
            Toast.makeText(this, "Service Stopped", Toast.LENGTH_SHORT).show()
//        binding.button.text = "Play"
            stopService(Intent(this, MusicService::class.java))
        } else {
//        binding.button.text = "Stop"
            Toast.makeText(this, "Service Started", Toast.LENGTH_SHORT).show()
            startService(Intent(this, MusicService::class.java))
        }
    }

    fun isMyServiceRunning(mClass: Class<MusicService>): Boolean {
        val manager: ActivityManager = getSystemService(
            Context.ACTIVITY_SERVICE
        ) as ActivityManager
        for (service: ActivityManager.RunningServiceInfo in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (mClass.name.equals(service.service.className)) {
                return true
            }
        }
        return false
    }
}
    */
