package com.rave.replicatingmusic.musicpanels

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.Layout
import com.rave.replicatingmusic.states.PlayerScreenState

@Composable
fun MusicPanels(
    modifier: Modifier,
//    screenState: PlayerScreenState,
    content: @Composable () -> Unit,
) {
    Layout(content, modifier) { measurables, constraints ->
        layout(constraints.maxWidth, constraints.maxHeight) {

            val playerControlConstraints = constraints.copy(
//                minHeight = screenState.playerContainerHeight,
                minHeight = 800,
//                maxHeight = screenState.playerContainerHeight
                maxHeight = 800
            )

            val songInfoContainerConstraints = constraints.copy(
//                minHeight = screenState.songInfoContainerHeight,
                minHeight = 1000,
//                maxHeight = screenState.songInfoContainerHeight,
                maxHeight = 1000,
            )

            require(measurables.size == 2)

            val playerControlContainer = measurables[1]
            val songInfoContainer = measurables[0]

            songInfoContainer.measure(songInfoContainerConstraints)
//                .place(0, screenState.songInfoOffset.toInt())
                .place(0, 200)
            playerControlContainer.measure(playerControlConstraints)
//                .place(0, screenState.playerControlOffset.toInt())
                .place(0, 100)

        }
    }
}