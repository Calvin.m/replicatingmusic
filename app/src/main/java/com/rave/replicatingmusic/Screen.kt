package com.rave.replicatingmusic

sealed class Screen(val route: String) {
    object MainScreen : Screen("Main Screen")
    object AlbumScreen : Screen("Album Screen")
}
