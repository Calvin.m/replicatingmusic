package com.rave.replicatingmusic.service

import android.app.*
import android.content.Intent
import android.media.MediaPlayer
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import com.rave.replicatingmusic.MainActivity
import com.rave.replicatingmusic.R

const val CHANNEL_ID = "1001"
const val MUSIC_NOTIFICATION_ID = 123

class MusicService : Service() {

    //    private val musicPlayer
    private lateinit var musicPlayer: MediaPlayer

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        initMusic()
        createNotificationChannel()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        showNotification()
        // play music in the background
        if (musicPlayer.isPlaying) {
            musicPlayer.stop()
        } else {
            musicPlayer.start()
        }

        return START_STICKY
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun showNotification() {
        val notificationIntent = Intent(this, MainActivity::class.java)

        val pendingIntent = PendingIntent.getActivity(
//            this, 0, notificationIntent, 0
            this, 0, notificationIntent, PendingIntent.FLAG_IMMUTABLE
        )

        val notification = Notification
            .Builder(this, CHANNEL_ID)
            .setContentText("Music Player")
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentIntent(pendingIntent)
            .build()

        println("$notification")

        startForeground(MUSIC_NOTIFICATION_ID, notification)
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            println("HELLLOOO????")

            val serviceChannel = NotificationChannel(
                CHANNEL_ID,
                "My Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )

            val manager = getSystemService(
                NotificationManager::class.java
            )

            manager.createNotificationChannel(serviceChannel)
        }
    }

    private fun initMusic() {
        musicPlayer = MediaPlayer.create(this, R.raw.musicapp)
        musicPlayer.isLooping = true
        musicPlayer.setVolume(100F, 100F)
    }

    override fun onDestroy() {
        super.onDestroy()
        musicPlayer.stop()
    }
}