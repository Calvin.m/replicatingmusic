package com.rave.replicatingmusic

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.leanback.widget.Row

@Composable
fun UserList(modifier: Modifier) {
    val users = listOf(
        User(
            R.drawable.ic_launcher_foreground,
            "John Doe",
            "Software Engineer",
            "Feb 13, 2023"
        ),
        User(
            R.drawable.ic_launcher_foreground,
            "Jane Smith",
            "UI Designer",
            "Feb 12, 2023"
        ),
        User(
            R.drawable.ic_launcher_foreground,
            "Bob Jones",
            "Software Engineer 2",
            "Feb 13, 2024"
        ),
        User(
            R.drawable.ic_launcher_foreground,
            "Jill Jones",
            "UI Designer 2",
            "Feb 12, 2024"
        ),
        User(
            R.drawable.ic_launcher_foreground,
            "John Doe",
            "Software Engineer",
            "Feb 13, 2023"
        ),
        User(
            R.drawable.ic_launcher_foreground,
            "Jane Smith",
            "UI Designer",
            "Feb 12, 2023"
        ),
        User(
            R.drawable.ic_launcher_foreground,
            "Bob Jones",
            "Software Engineer 2",
            "Feb 13, 2024"
        ),
        User(
            R.drawable.ic_launcher_foreground,
            "Jill Jones",
            "UI Designer 2",
            "Feb 12, 2024"
        ),
        // Add more users as needed
    )

    LazyColumn(
        modifier = Modifier.fillMaxSize()
    ) {
        items(users) { user ->
            UserItem(user)
        }
    }
}

@Composable
fun UserItem(user: User) {
    Row(
        modifier = Modifier
            .padding(11.dp)
            .fillMaxWidth()
            .background(Color(55,55,55)),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Image(
            painter = painterResource(user.profileImage),
            contentDescription = null,
            modifier = Modifier.size(50.dp)
        )
        Column(
            modifier = Modifier
                .padding(start = 16.dp)
                .weight(1f)
        ) {
            Text(
                text = user.name,
                style = MaterialTheme.typography.subtitle1,
                color = Color.White
            )
            Text(
                text = user.text,
                style = MaterialTheme.typography.body2,
                color = Color.White
            )
        }
        Text(
            text = user.date,
            style = MaterialTheme.typography.body2,
            color = Color.White,
            modifier = Modifier.padding(end = 12.dp)
        )
    }
}

data class User(
    @DrawableRes val profileImage: Int,
    val name: String,
    val text: String,
    val date: String
)